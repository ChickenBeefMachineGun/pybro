#!/usr/bin/env python3
# pybro
#
# Copyright 2020 Valio Valtokari
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	<http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
import os
import signal
import gettext

VERSION = '@VERSION@'
pkgdatadir = '@pkgdatadir@'
localedir = '@localedir@'

sys.path.insert(1, pkgdatadir)
signal.signal(signal.SIGINT, signal.SIG_DFL)
gettext.install('pybro', localedir)

if __name__ == '__main__':

    import gi
    gi.require_version('Gio', '2.0')

    from gi.repository import Gio

    resource = Gio.Resource.load(os.path.join(pkgdatadir, 'pybro.gresource'))
    resource._register()

    from pybro import main
    sys.exit(main.main(VERSION))

