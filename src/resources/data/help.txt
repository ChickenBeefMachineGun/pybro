PyBro v1.0. Copyright (c) Valio Valtokari 2020.
Licensed under the Apache license, version 2.0.
$$
Usage: pybro [OPTIONS], all optional

Available arguments:
    General:
        -h, --help              Show this help message and exit
        --version, -v           Show some info about the program (like the version number) and exit
    Logging options:
        --logfile=FILE, -l      Redirect logging to FILE
        --redirect-log=FILE,
        -r=FILE                 Redirect logging to FILE, permanently
        --log-level=LEVEL,
        -ll=LEVEL               Set log level to LEVEL. Possible options are
                                'debug', 'info', 'warning', 'error' and 'critical'
        --verbose, -vb          Verbose mode. Equivalent to -ll=DEBUG
    Navigation:
        --url=URI, -u=URI       Open the URI
      
