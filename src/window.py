#!@PYTHON@
# window.py
#
# Copyright 2020 Valio Valtokari
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	    <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import gi
import os
import tempfile

gi.require_version('Gtk', '3.0')
gi.require_version('GObject', '2.0')
gi.require_version('Gio', '2.0')

from gi.repository import Gtk, GObject, Gio
from .widgets import PyBroWebView

@Gtk.Template(resource_path='/com/vipurapu/pybro/window.ui')
class PyBroWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'PyBroWindow'
    def __prep(self, resourcepath: str):
        # TODO: make this less ugly
        self.headerbar_button_names = ["search", "next-button", "back-button", "refresh-button"]
        self.headerbar_buttons = []
        builder = Gtk.Builder.new_from_resource(resourcepath)
        self.port = builder.get_object('port')
        self.search = builder.get_object('search')
        self.browser = PyBroWebView()
        self.port.add(self.browser)
        self.add(builder.get_object('browser-holder'))
        self.headerbar = builder.get_object('header-bar')
        for widget in self.headerbar_button_names:
            self.headerbar_buttons.append(builder.get_object(widget))
        self.set_titlebar(self.headerbar)
        self.show_all()
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        GObject.type_register(PyBroWebView)
        self.__prep('/com/chickenbeefmachinegun/pybro/widgets.ui')
