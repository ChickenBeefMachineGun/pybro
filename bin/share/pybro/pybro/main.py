# main.py
#
# Copyright 2020 Valio Valtokari
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	    <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
import gi
import os

gi.require_version('Gtk', '3.0')
gi.require_version('Gio', '2.0')
gi.require_version('GLib', '2.0')
gi.require_version('WebKit2', '4.0')

from gi.repository import Gtk, Gio, GLib, WebKit2 as WebKit

from .logger import log, LogLevel
from .read_resource import read_resource_data
from .window import PyBroWindow
from .controller import Contoller, Handler

pkgdatadir = '/home/valio/Projektit/PyBro/bin/share/pybro'

class Application(Gtk.Application):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, application_id='com.vipurapu.pybro',
                         flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE, **kwargs)
        self.add_main_option("uri",
            ord("u"),
            GLib.OptionFlags.NONE,
            GLib.OptionArg.STRING,
            "Open the specified URI",
            'URI',
        )
        self.settings = Gio.Settings('com.vipurapu.pybro')

    def do_command_line(self, cmdline):
        options = cmdline.get_options_dict()
        options = options.end().unpack()
        self.do_activate()
        return 0

    def do_activate(self):
        win = self.props.active_window
        if not win:
            win = PyBroWindow(application=self)
        self.prepare(win)

    def prepare(self, window: Gtk.Window):
        self.browser = window.browser
        self.search = window.search
        self.browser.load_uri(self.settings.get_string('homepage'))
        self.browser.connect('load-changed', self.load_changed)
        self.browser.connect('load-failed', self.load_failed)
        buttons = window.headerbar_buttons
        search = buttons[0]
        next = buttons[1]
        back = buttons[2]
        refresh = buttons[3]
        back.connect("clicked", self.go_back)

    def go_back(self, widget, user_data=None):
        self.browser.go_back()

    def load_changed(self, widget, event, user_data=None):
        Handler.load_changed(self, widget, event, user_data=user_data)

    def pulse(self, data):
        Handler.pulse(self, data)

    def progress_0(self, *args, **kwargs):
        Handler.progress_0(self, *args, **kwargs)

    def load_failed(self, widget, event, ignored, user_data=None):
        Handler.load_failed(self, widget, event, user_data=user_data, datadir=pkgdatadir)

def main(version):
    app = Application()
    return app.run(sys.argv)
