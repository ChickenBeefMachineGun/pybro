# controller.py
#
# Copyright 2020 Valio Valtokari
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	    <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import gi
import os

gi.require_version('WebKit2', '4.0')
gi.require_version('GLib', '2.0')

from gi.repository import WebKit2 as WebKit
from gi.repository import GLib

from .read_resource import read_resource_data
from .logger import log, LogLevel

class Contoller:
    def __init__(self, webview: WebKit.WebView, settings=WebKit.Settings()):
        self._view = webview
        self._prefs = settings

class Handler:
    def __init__(self):
        raise RuntimeError('no')

    def load_changed(self, widget, event, user_data=None):
        if event == WebKit.LoadEvent.STARTED:
            self.search.set_progress_fraction(.25)
            log('loading started for page \'{}\''.format(widget.get_uri()[:60] + '...'))
        elif event == WebKit.LoadEvent.FINISHED:
            self.search.set_progress_fraction(1.0)
            log('done loading page \'{}\''.format(widget.get_uri()[:60] + '...'))
            GLib.timeout_add(1, self.progress_0, False)
        elif event == WebKit.LoadEvent.REDIRECTED:
            self.search.set_progress_fraction(.50)
        elif event == WebKit.LoadEvent.COMMITTED:
            self.search.set_progress_fraction(.75)
            log('loading page \'{}\''.format(widget.get_uri()[:60] + '...'))

    def progress_0(self, data):
        self.search.set_progress_fraction(0)

    def load_failed(self, widget, event, user_data=None, datadir=None):
        log('loading failed for page \'{}\''.format(widget.get_uri()), level=LogLevel.CRITICAL)
        html = read_resource_data(os.path.join(datadir, 'pybro.gresource'), '/com/vipurapu/pybro/error-page').decode().replace('{%CODE%}', '404')
        self.browser.load_html(html, None)
