# widgets.py
#
# Copyright 2020 Valio Valtokari
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	    <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('WebKit2', '4.0')

from gi.repository import Gtk, WebKit2 as WebKit

class WebViewFlags:
    HOME = 0
    GOOGLE = 1
    DDGO = 2
    BING = 3
    YAHOO = 4
    YANDEX = 5
    BAIDU = 6
    CUSTOM = None
    def custom(url: str):
        self.CUSTOM = url

class PyBroWebView(WebKit.WebView):
    def __init__(self, startpage=WebViewFlags.HOME, settings=None):
        super().__init__()
        if settings is None:
            self.settings = WebKit.Settings()
        else:
            self.settings = settings
