# read_resource.py
#
# Copyright 2020 Valio Valtokari
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	    <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import gi

gi.require_version('Gio', '2.0')

from gi.repository import Gio

def read_resource_data(resource_file_path: str, data_path: str):
    resource = Gio.Resource.load(resource_file_path)
    flags = Gio.ResourceLookupFlags.NONE
    res_stream = resource.open_stream(data_path, flags)
    if not res_stream:
        err = 'Resource \'{}\' from \'{}\' could not be loaded'.format(icon_path, resource_file_path)
        raise GLib.Error(err)
    DATA = res_stream.read_bytes(resource.get_info(data_path, flags).size, None).get_data()
    return DATA
