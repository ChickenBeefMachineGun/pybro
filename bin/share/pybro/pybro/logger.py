# logger.py
#
# Copyright 2020 Valio Valtokari
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	    <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
import enum
from termcolor import cprint

class LogLevel(enum.Enum):
    DEFAULT = 0
    WARNING = 1
    CRITICAL = 2

def log(msg, level=LogLevel.DEFAULT):
    if level == LogLevel.DEFAULT:
        cprint('[LOG]: ' + msg, color='green', attrs=['bold'])
    elif level == LogLevel.WARNING:
        cprint('[WARNING]: ' + msg, color='blue', attrs=['bold'])
    elif level == LogLevel.CRITICAL:
        cprint('[CRITICAL]: ' + msg, color='red', attrs=['bold'])

# TODO: implement a logger
class PyBroLogger:
    pass
