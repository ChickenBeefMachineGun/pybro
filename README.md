# NOTE:
this is unusable for now. many files are still half made, this is in a very early development phase. 

# PyBro

PyBro is a simple browser using the GTK+ toolkit and WebKit2Gtk 4.0, in pure Python 3! It is still in a very early development phase, but I wish to make it a completely usable web browser!


# TODO:
* Logging
* History
* Custom history html page
* Synchronization (Google, Firefox Sync?)
* Ad blocking
* Custom ```alert()``` popup
* Downloads
* Everything else. It's literally got a next, back and refresh button now.


# Contributing
I don't think this is so big of a project that anyone would want to contribute, but if you want to, reach out to me at ```ubuntugeek1904@gmail.com``` and we'll discuss it.


#### :)
